

.. image:: https://img.shields.io/gitlab/pipeline/gyptis/docker/master?style=for-the-badge
   :target: https://gitlab.com/gyptis/docker/commits/master
   :alt: pipeline status

.. image:: https://img.shields.io/docker/v/gyptis/gyptis?color=8678bd&label=dockerhub&logo=docker&logoColor=white&style=for-the-badge
  :target: https://hub.docker.com/repository/docker/gyptis/gyptis
  :alt: dockerhub

.. image:: https://img.shields.io/docker/pulls/gyptis/gyptis?color=8678bd&style=for-the-badge
  :alt: docker pulls

.. image:: https://img.shields.io/docker/image-size/gyptis/gyptis?color=8678bd&style=for-the-badge
  :alt: docker image size
  
.. image:: https://img.shields.io/badge/license-MIT-blue?color=bb798f&style=for-the-badge
  :target: https://gitlab.com/gyptis/docker/-/blob/master/LICENCE.txt
  :alt: license



Docker for Gyptis
=================

Containers with the Gyptis code and development tools.

.. figure:: https://gitlab.com/gyptis/docker/-/raw/master/screenshot.svg
  :alt: screenshot
  :width: 100%
